package main

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	host     = "121.37.15.94"
	port     = 5432
	user     = "postgres"
	password = "Joson.080256/"
	dbname   = "chitchat"
)

type UserInfo struct {
	id    int
	name  string
	email string
}

func main() {

	var JAVAHOME string
	JAVAHOME = os.Getenv("JAVA_HOME")

	fmt.Printf("\n")

	fmt.Printf("JAVAHOME:")
	fmt.Println(JAVAHOME)
	fmt.Println("hello, world")
	fmt.Printf("hello, world\n")

	fmt.Println("\n")
	var now time.Time
	now = time.Now()
	fmt.Println(now)

	fmt.Println(time.Now().Format("2006-01-02 15:04"))

	fmt.Println("\n")

	connectData()

}

func connectData() {

	//Go中使用PostgreSQL

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println(psqlInfo)
	fmt.Println("Successfully connected!")

	/*******************************************************/

	var (
		name string
		age  int
	)

	fmt.Print("输入姓名和年龄，使用空格分隔：")
	fmt.Scanln(&name, &age)
	fmt.Printf("name: %s\nage: %d\n", name, age)

	/*******************************************************/

	//"在rows.Scan时，传入的参数个数必须与SELECT返回的字段个数一致，否则会报错"
	//panic: sql: expected 6 destination arguments in Scan, not 3

	sqlStatement := `SELECT id,name,email FROM users WHERE name=$1;`
	var userInfo UserInfo
	row := db.QueryRow(sqlStatement, "Joson")
	err = row.Scan(&userInfo.id, &userInfo.name, &userInfo.email)

	switch err {
	case sql.ErrNoRows:
		fmt.Println()
		fmt.Println("No rows were returned!")
		return
	case nil:
		fmt.Println()
		fmt.Println(userInfo)
	default:
		panic(err)
	}

	fmt.Print("按任意键退出")
	fmt.Scanln()

}
